# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

# from typing import Any, Text, Dict, List
#
# from rasa_sdk import Action, Tracker
# from rasa_sdk.executor import CollectingDispatcher
#
#
# class ActionHelloWorld(Action):
#
#     def name(self) -> Text:
#         return "action_hello_world"
#
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#
#         dispatcher.utter_message(text="Hello World!")
#
#         return []

from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.events import EventType
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict
from rasa_sdk.events import SlotSet

ALLOWED_EMMISION = ["euro 0", "euro 1", "euro 2", "euro 3", "euro 4", "euro 5", "euro 6", "euro0", "euro1", "euro2", "euro3", "euro4", "euro5", "euro6"]
ALLOWED_WEIGHT = ["7.5-11.99t", "12-18t", ">18t","7.5 - 11.99t", "12 - 18t", "> 18t"]
ALLOWED_AXEL = ["3 or less", "4 or more"]
EMMISION_CLASS =["Euro 6", "Euro 5", "Euro 4", "Euro 3", "Euro 2", "Euro 1", "Euro 0"]
WEIGHT_CLASS = ["7.5-11.99t", "12-18t", ">18t"]
AXEL_CLASS =["3 or less", "4 or more"]

class ValidateCalcRateForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_calc_rate_form"

    def validate_emmision(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `emmision` value."""
        print('woop')

        if slot_value.lower() not in ALLOWED_EMMISION:
            dispatcher.utter_message(text=f"Please enter your Euro emmision class from 0 to 6. For example Euro 4")
            return {"emmision": None}
        dispatcher.utter_message(text=f"OK! Your vehicle has a {slot_value} emmision class.")
        return {"emmision": slot_value}

    def validate_axel(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `axel` value."""
        print('poop')

        if slot_value.lower() not in ALLOWED_AXEL:
            dispatcher.utter_message(text=f"I don't recognize the axel class. You should enter one of the following: {'/'.join(ALLOWED_AXEL)}.")
            return {"axel": None}
        dispatcher.utter_message(text=f"OK! Your vehicle has a {slot_value} axel class.")
        return {"axel": slot_value}

    def validate_weight(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `weight` value."""
        print('kack')

        if slot_value.lower() not in ALLOWED_WEIGHT:
            dispatcher.utter_message(text=f"I don't recognize the weight class. You should enter one of the following: {'/'.join(ALLOWED_WEIGHT)}.")
            return {"weight": None}
        dispatcher.utter_message(text=f"OK! Your vehicle has a {slot_value} weight class.")
        return {"weight": slot_value}




class AskForEmmisionAction(Action):
    def name(self) -> Text:
        return "action_ask_emmision"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        dispatcher.utter_message(
            text="What is the emmision class of your truck?",
            buttons=[{"title": p, "payload": p} for p in EMMISION_CLASS],
        )
        return []

class AskForAxelAction(Action):
    def name(self) -> Text:
        return "action_ask_axel"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        dispatcher.utter_message(
            text="What is the axel class of your truck?",
            buttons=[{"title": p, "payload": p} for p in AXEL_CLASS],
        )
        return []

class AskForWeightAction(Action):
    def name(self) -> Text:
        return "action_ask_weight"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        dispatcher.utter_message(
            text="What is the weight class of your truck?",
            buttons=[{"title": p, "payload": p} for p in WEIGHT_CLASS],
        )
        return []

class ActionCalcTollRate(Action):

    def name(self) -> Text:
        return "action_calc_toll_rate"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        em = tracker.get_slot("emmision")
        ax = tracker.get_slot("axel")
        we = tracker.get_slot("weight")

        rate = 0
        infra = 0
        
        if em.lower() == "euro 6" and we.lower() == "7.5-11.99":
            rate = 7.9
            infra = 6.5
            msg = f"The toll rate for a vehicle with {em} emmision and {we} weight class with {ax} axels is {rate} cents/km with the proportion for infrastructure {infra} cents/km"
            dispatcher.utter_message(text=msg)
            return [SlotSet("emmision", None),SlotSet("axel", None), SlotSet("weight", None)]
        if em.lower() == "euro 6" and we.lower() == "12-18t":
            rate = 12.6
            infra = 11.2
            msg = f"The toll rate for a vehicle with {em} emmision and {we} weight class with {ax} axels is {rate} cents/km with the proportion for infrastructure {infra} cents/km"
            dispatcher.utter_message(text=msg)
            return [SlotSet("emmision", None),SlotSet("axel", None), SlotSet("weight", None)]
        if em.lower() == "euro 6" and we.lower() == ">18t" and ax.lower() == "3 or less":
            rate = 16.9
            infra = 15.5
            msg = f"The toll rate for a vehicle with {em} emmision and {we} weight class with {ax} axels is {rate} cents/km with the proportion for infrastructure {infra} cents/km"
            dispatcher.utter_message(text=msg)
            return [SlotSet("emmision", None),SlotSet("axel", None), SlotSet("weight", None)]
        if em.lower() == "euro 6" and we.lower() == ">18t" and ax.lower() == "4 or more":
            rate = 18.3
            infra = 16.9
            msg = f"The toll rate for a vehicle with {em} emmision and {we} weight class with {ax} axels is {rate} cents/km with the proportion for infrastructure {infra} cents/km"
            dispatcher.utter_message(text=msg)
            return [SlotSet("emmision", None),SlotSet("axel", None), SlotSet("weight", None)]
        if em.lower() == "euro 5" and we.lower() == "7.5-11.99":
            rate = 9.0
            infra = 6.5
            msg = f"The toll rate for a vehicle with {em} emmision and {we} weight class with {ax} axels is {rate} cents/km with the proportion for infrastructure {infra} cents/km"
            dispatcher.utter_message(text=msg)
            return [SlotSet("emmision", None),SlotSet("axel", None), SlotSet("weight", None)]
        if em.lower() == "euro 5" and we.lower() == "12-18t":
            rate = 13.7
            infra = 11.2
            msg = f"The toll rate for a vehicle with {em} emmision and {we} weight class with {ax} axels is {rate} cents/km with the proportion for infrastructure {infra} cents/km"
            dispatcher.utter_message(text=msg)
            return [SlotSet("emmision", None),SlotSet("axel", None), SlotSet("weight", None)]
        if em.lower() == "euro 5" and we.lower() == ">18t" and ax.lower() == "3 or less":
            rate = 18.0
            infra = 15.5
            msg = f"The toll rate for a vehicle with {em} emmision and {we} weight class with {ax} axels is {rate} cents/km with the proportion for infrastructure {infra} cents/km"
            dispatcher.utter_message(text=msg)
            return [SlotSet("emmision", None),SlotSet("axel", None), SlotSet("weight", None)]
        if em.lower() == "euro 5" and we.lower() == ">18t" and ax.lower() == "4 or more":
            rate = 19.4
            infra = 16.9
            msg = f"The toll rate for a vehicle with {em} emmision and {we} weight class with {ax} axels is {rate} cents/km with the proportion for infrastructure {infra} cents/km"
            dispatcher.utter_message(text=msg)
            return [SlotSet("emmision", None),SlotSet("axel", None), SlotSet("weight", None)]
        else:
            msg = "Something went wrong :("
            dispatcher.utter_message(text=msg)
            return [SlotSet("emmision", None),SlotSet("axel", None), SlotSet("weight", None)]

        
        